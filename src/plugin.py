from discord.ext import commands

plugins = []
help = []


def add_plugin(name, func, help):
    plugins.append((name, func, help))


def dispatch(bot):
    for fname, func, fhelp in plugins:
        f=commands.Command(func, name=fname, help=fhelp)
        bot.add_command(f)
