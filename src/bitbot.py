import os, sys, glob, importlib
import discord
from discord.ext import commands
from dotenv import load_dotenv

import plugin

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

class BitBot(commands.Bot):
    def __init__(self):
        intents = discord.Intents(guilds=True, messages=True, reactions=True, guild_reactions=True, guild_typing=True)
        super().__init__(command_prefix='!', intents=intents)

        plug_dir = os.path.dirname(os.path.abspath(__file__)) + '/plugins/'
        sys.path.append(plug_dir)
        files = glob.glob(plug_dir + '*.py')
        plugs = []
        for f in files:
            name = os.path.basename(f)
            plugs.append(importlib.import_module(name[:-3]))
        print(plugs)

    def run(self):
        plugin.dispatch(self)
        super().run(TOKEN)

    async def on_ready(self):
        print('Connected to Discord!')

bot = BitBot()
bot.run()
