import random
import plugin


async def eightball(ctx):
    items = ctx.message.content.split(' ')
    items = [item for item in items if item != '' and item != '!8ball']
    nick = "<@"+str(ctx.author.id)+">: "

    if len(items) < 1:  # no questions to answer...
        await ctx.send(nick+"Please ask a question!")
    else:
        answers = [
            'It is certain',
            'It is decidedly so',
            'Without a doubt',
            'Yes definitely',
            'You may rely on it',
            'As I see it, yes',
            'Most likely',
            'Outlook good',
            'Yes',
            'Signs point to yes',
            'Reply hazy try again',
            'Ask again later',
            'Better not tell you now',
            'Cannot predict now',
            'Concentrate and ask again',
            'Don\'t count on it',
            'My reply is no',
            'My sources say no',
            'Outlook not so good',
            'Very doubtful',
        ]

        rnd = random.randint(0, len(answers) - 1)
        await ctx.send(nick + answers[rnd])


helpmsg = 'The Magic 8-Ball is a toy used for fortune-telling or seeking advice, developed in the 1950s. Example: !8ball Will I have a great day?'
plugin.add_plugin('8ball', eightball, helpmsg)
