import requests
import plugin

XKCD_LATEST_URL = 'https://xkcd.com/'
XKCD_JSON_URL = 'https://xkcd.com/info.0.json'


def fetch_latest_xkcd():
    response = requests.get(XKCD_JSON_URL)
    if response.status_code == 200:
        data = response.json()
        return data['num'], data['title'], data['alt'], XKCD_LATEST_URL + str(data['num'])
    return None, None, None, None


async def xkcd(ctx):
    msg = ctx.message.content

    if msg.startswith('!xkcd'):
        num, title, alt_text, link = fetch_latest_xkcd()
        if num and title and alt_text:
            response = 'Latest xkcd - #%d: %s | Link: %s | Alt text: %s' % (num, title, link, alt_text)
            await ctx.send(response)


plugin.add_plugin('xkcd', xkcd, 'Latest xkcd comic. Usage: !xkcd')
