import plugin

from youtubesearchpython import *
import random

async def youtube_search(ctx):
    msg = ctx.message.content
    _cmd, title = msg.split(None, 1)

    search = Search(title, limit = 20)
    rnd = random.randint(0, len(search.result()['result']) - 1)
    result = (search.result()['result'][rnd]['link']+" - "+search.result()['result'][rnd]['title'])

    await ctx.send(result)




helpmsg = 'Search for a youtube video and select a *random* from the search result.'
plugin.add_plugin('yt', youtube_search, helpmsg)
