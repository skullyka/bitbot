import plugin

import requests
import xml.etree.ElementTree as ET
import html


class MNBExchangeRateFetcher:
    def __init__(self, url):
        self.url = url
        self.headers = {'Content-Type': 'application/soap+xml; charset=utf-8'}
        self.body = """<?xml version="1.0" encoding="UTF-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mnb="http://www.mnb.hu/webservices/">
           <soap:Header/>
           <soap:Body>
              <mnb:GetCurrentExchangeRates/>
           </soap:Body>
        </soap:Envelope>"""

    def send_soap_request(self):
        return requests.post(self.url, data=self.body, headers=self.headers, timeout=10)

    def parse_xml_response(self, response):
        decoded_response = html.unescape(response.text)
        start_tag = '<GetCurrentExchangeRatesResult>'
        end_tag = '</GetCurrentExchangeRatesResult>'
        start = decoded_response.find(start_tag) + len(start_tag)
        end = decoded_response.find(end_tag)
        xml_content = decoded_response[start:end]
        return ET.fromstring('<root>' + xml_content + '</root>')

    def find_currency_rate(self, root, currency_code):
        return root.find(f".//Rate[@curr='{currency_code.upper()}']")

    def fetch_exchange_rate_for_currency(self, currency_code, amount=1):
        response = self.send_soap_request()
    
        if response.status_code == 200:
            root = self.parse_xml_response(response)
            rate = self.find_currency_rate(root, currency_code)
    
            if rate is not None:
                unit = int(rate.attrib['unit'])
                value = float(rate.text.replace(',', '.'))
    
                converted_amount = (amount / unit) * value
    
                exchange_rate = f"{amount} {currency_code.upper()} = {converted_amount:.2f} HUF"
                return exchange_rate
            else:
                raise Exception(f"Nem található árfolyam a '{currency_code}' valutához.")
        else:
            raise Exception(f"Hiba történt az adatok lekérdezésekor: {response.status_code}")


async def mnb(ctx):
    msg = ctx.message.content
    items = msg.split(' ')
    # return if there are no choices to choose from
    if len(items) != 2 and len(items) != 3:
        await ctx.send("Nem megfelelő számú paraméter!")
        return
    # remove empty "items"
    items = [item for item in items if item != '']
    url = 'http://www.mnb.hu/arfolyamok.asmx?wsdl'
    fetcher = MNBExchangeRateFetcher(url)
    try:
        if len(items) == 2:
            currency_code = items[1]
            exchange_rate = fetcher.fetch_exchange_rate_for_currency(currency_code)
        elif len(items) == 3:
            ammount = int(items[1])
            currency_code = items[2]
            exchange_rate = fetcher.fetch_exchange_rate_for_currency(currency_code, ammount)
        await ctx.send(exchange_rate)
    except Exception as e:
        await ctx.send("Hiba történt!")
        print(e)


plugin.add_plugin('mnb', mnb,
                'Print MNB exchange rate. Example: !mnb EUR')

