import subprocess

import plugin


async def fortune(ctx):
    try:
        output = subprocess.check_output('fortune').decode()
    except Exception as e:
        await ctx.send('Error executing "fortune":' % e)
    await ctx.send(output)


plugin.add_plugin('fortune', fortune, 'Provide a random fortune')
