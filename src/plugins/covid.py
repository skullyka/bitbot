import os
import urllib


import plugin

import numpy as np
import pandas as pd

async def covid(ctx):
    nick = "<@"+str(ctx.author.id)+">: "

    url = 'https://docs.google.com/spreadsheets/d/1e4VEZL1xvsALoOIq9V2SQuICeQrT5MtWfBm32ad7i8Q/export?format=csv'
    filename = '/tmp/covidstat.csv'
    try:
        urllib.request.urlretrieve(url, filename) #get hash, if different, update
    except urllib.error.HTTPError:
        msg = "HTTPError, Try again later! (ping skullyka)"
    else:
        df = pd.read_csv(filename)
        msg = create_msg(df)

    for m in msg:
        await ctx.send(nick + "\n```" +m+"```")

def create_msg(df):
    days = days_of_data(df)
    msg_whole = df.iloc[-1].to_string()
    if days > 0:
        msg_whole += "\n"+ "Ezelőtt " + str(days) + " napig nem volt adat."
    else:
        msg_whole += "\n\nNincs csúszó adat."

    msg=["\n".join(msg_whole.split('\n')[:20]),"\n".join(msg_whole.split('\n')[20:])]
    return msg


def days_of_data(df):
    days = 0
    i=-2
    while(pd.isnull(df.iloc[i].Elhunytak)):
        print(df.iloc[i].Dátum)
        days+=1
        i-=1
    return days


plugin.add_plugin('covid',covid,
                'Hungarian covid stats')
