from easy_exchange_rates import API
from datetime import datetime

import plugin


async def exch(ctx):
    msg = ctx.message.content
    ex = msg.split()[1:]
    l_ex = len(ex)
    output = ''
    currencies = ["EUR", "USD", "JPY", "BGN", "CZK", "DKK", "HUF", "GBP", "PLN", "RON", "SEK", "CHF", "NOK",
                  "HRK", "TRY", "AUD", "CAD", "BTC"]
    exch = [i.upper() for i in ex]

    if l_ex == 0:
        output = "what to what?"

    if l_ex == 1:  # exchange_rate
        rates = get_rate(base=exch[0], target=currencies)
        output = gen_output(1, exch[0], rates)

    if l_ex == 2:  # 1 stg to stg
        try:
            amm = get_rate(base=exch[0], target=exch[1])
            output = gen_output(1, exch[0], (exch[1], amm))
        except KeyError:
            output = "I dont like this (KeyError thrown)"

    if l_ex == 3:  # x stg to stg
        try:
            amm = get_rate(base=exch[1], target=exch[2], amount=float(exch[0]))
            output = gen_output(exch[0], exch[1], (exch[2], amm))
        except KeyError:
            output = "I dont like this (KeyError thrown)"

    if l_ex > 3:
        output = "this is too much"

    await ctx.send(output)


def get_rate(amount=1.0, base="HUF", target="EUR"):
    api = API()
    today = datetime.today().strftime('%Y-%m-%d')
    if isinstance(target, list):
        rate = api.get_exchange_rates(base_currency=base, targets=target, start_date=today, end_date=today)
        print(rate)
        if base in rate[today]:
            del rate[today][base]
        return rate[today]
    else:
        rate = api.get_exchange_rates(base_currency=base, targets=[target], start_date=today, end_date=today)
        ret = rate[today][target]
        return amount * ret


def gen_output(src_amm, src_curr, dest):
    if isinstance(dest, dict):
        output = str(src_amm) + " " + src_curr + " = \n"
        for currency, rate in dest.items():
            output += str(rate) + " " + currency + '\n'
    else:
        output = str(src_amm) + " " + src_curr + " = " + str(dest[1]) + " " + dest[0]

    return output


plugin.add_plugin('exch', exch, "Exchange rates Example: !exch BTC, !exch 5 USD HUF, !exch EUR HUF")
