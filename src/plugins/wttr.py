import requests
import plugin


async def wttr(ctx):

    msg = ctx.message.content

    try:
        cmd, city = msg.split(None, 1)
    except ValueError:
        city = "Budapest"
        cmd = msg
    nick = "<@"+str(ctx.author.id)+">: "

    if cmd == '!wttr':
        formatstr = '%l,+%T(%Z):\n' + \
                    '+%c+%C\n' + \
                    '%t, Feels Like: %f\n' + \
                    'Humidity: %h\n' + \
                    'Precipitation: %p, Pressure: %P\n' + \
                    '%w+%m+Moon day: %M\n' + \
                    'Dawn: %D,+Sunrise: %S,+Zenith: %z,+Dusk: %d'
        url = "http://wttr.in/" + city + '?m&format="' + formatstr + '"'
        req = requests.get(url)

        await ctx.send(nick + "\n" + req.text[1:-1])
    else:
        url = "http://wttr.in/"+ city +"?mqTFn"
        req = requests.get(url)
        textlist = req.text.replace('`', '\'').split('\n')

        await ctx.send(nick + "\n`" + "\n".join(textlist[0:17])+"`")
        await ctx.send(nick + "\n`" + "\n".join(textlist[17:])+"`")


plugin.add_plugin('wttr', wttr, 'Datarich weather forecast using wttr.it Example: !wttr Budapest')
plugin.add_plugin('awttr', wttr, 'Ascii weather forecast using wttr.it Example: !awttr Budapest')

