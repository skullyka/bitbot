import base64
import plugin


async def b64(ctx):
    cmd, text = ctx.message.content.split(None, 1)
    # Lazy
    if cmd == '!b64d':
        result = base64.b64decode(text)
    else:
        data_bytes = text.encode("utf-8")
        result = base64.b64encode(data_bytes)
    await ctx.send('Base64: %s ' % result.decode("utf-8"))
    return


plugin.add_plugin('b64', b64, 'Base64 encodes the argument. Example: !b64 foo')
plugin.add_plugin('b64d', b64, 'Base64 decodes the argument. Example: !b64d Zm9v==')
