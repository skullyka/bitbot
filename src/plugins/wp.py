import wikipediaapi

import plugin


async def wikipedia_lookup(ctx):
    msg = ctx.message.content
    _cmd, title = msg.split(None, 1)
    wiki_wiki = wikipediaapi.Wikipedia('en')

    page_py=wiki_wiki.page(title)
    if not page_py.exists():
        page_py=wiki_wiki.page(title.title())
    if page_py.exists():
        if 'Category:All article disambiguation pages' in page_py.categories:
            p = []
            for k,_ in list(page_py.links.items())[0:9]:
                p.append(k)

            pages = '|'.join(p)
            results = '"%s" may refer to: %s' % (title, pages)
            await ctx.send(results)
            return

        else:
           page = page_py.fullurl
    else:
        line = 'No such page: %s (or %s)' % (title, title.title())
        await ctx.send(line)
        return

    await ctx.send(page)


plugin.add_plugin('wp', wikipedia_lookup, 'Query Wikipedia. Example: !wp ozric tentacles')
